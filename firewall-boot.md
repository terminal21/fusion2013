# Boot Bug CaptivePortal

Bug: Die Firewall startet das CaptivePortal nicht automatisch. Kein CaptivePortal == Halbes Internet.

## So wird das CaptivePortal per Hand gestartet

`ssh 192.168.21.1 -l root` (Hint: Terminal.21 FireWall)

`su - portal`

`./bin/supervisord`