# OpenBSD Hints

## DHCP

* config: /etc/dhcpd.conf
* network binds: /etc/rc.conf.local
* restart: /etc/rc.d/dhcpd restart

## Firewall

* config: /etc/pf.conf
* test config (YES, TEST IT!!!): pfctl -nf /etc/pf.conf
* apply: pfctl -f /etc/pf.conf
* doku: http://www.openbsd.org/faq/pf/de/

## CaptivePortal

* Inspect /srv/portal
* python, virtualenv, bottle, supervisor