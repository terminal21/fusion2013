# CaptivePortal Management

Das CaptivePortal benutzt Tabellen des OpenBSD PortFilters. Bei jedem DHCP-Lease wird die vergebene IP-Adresse automatisch in der Tabelle <newclients> eingetragen. HTTP-Zugriffe mit IPs aus <newclients> werden auf das CaptivePortal umgeleitet. Mit dem Klick auf den Button im CaptivePortal wird der Eintrag in <newclients> entfernt und der HTTP-Zugriff freigegeben. Alle anderen Ports sind immer freigegeben.

## Tabelle <newclients> verwalten

`ssh 192.168.21.1 -l root` (Hint: Terminal.21 FireWall)

`pfctl -t newclients -T show` zeigt alle Clients, die eingewählt, aber nicht freigeschaltet sind

`pfctl -t newclients -T add IP` zwingt einen Client sich am CaptivePortal anzumelden

`pfctl -t newclients -T delete IP` schaltet einen Client am CaptivePortal frei