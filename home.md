[[_TOC_]]

Technische Doku Fusion 2013
===========================

Gollum Wiki
-----------
[installation](/gollum-install)

[wiki syntax](http://daringfireball.net/projects/markdown/)

Firewall
--------

[booting](/firewall-boot)

[captive portal](firewall-captiveportal)

[setup hints](firewall-hints)

ThinClients
-----------

[restart](/how-to-restart-a-frozen-thin-client)

DNS / Cashing Proxy (HTTP) / PictureWrap
----------------------------------------

IPs: 192.168.23.[23,42] <br />
"fake" DNS: 192.168.23.23:53 <br />
"real" DNS: 192.168.23.42:53

try: 

    dig @192.168.23.23 doubleclick.net

and compare with:

    dig @192.168.23.42 doubleclick.net 

Im DHCP wird also 192.168.23.23 an die Clients serviert, der fuer von uns festgelegten Ad-Domains die IP unseres lokalen Servers zurueckliefert.

[Details](/dns-settings)


Satellitenfoo
=============

nochwas
-------

filiago
-------

 * modem IP: 192.168.0.1/24 (avanti)
 * remote IP: 192.168.50.18
 * .. broken