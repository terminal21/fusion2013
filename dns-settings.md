[[_TOC_]]

Overview Services
=================

    ssh -p2222 user@192.168.23.23

    netstat -tulpen | grep :53
    root@troxy:/home/phaidros# netstat -ulpen | grep :53
    udp        0      0 127.0.0.1:53            0.0.0.0:*                           0          6518        2452/dnscache   
    udp        0      0 192.168.23.23:53        0.0.0.0:*                           0          6516        2448/dnscache   
    udp        0      0 127.0.0.53:53           0.0.0.0:*                           0          6515        2447/tinydns    
    udp        0      0 127.0.0.2:53            0.0.0.0:*                           0          6514        2454/tinydns    
    udp        0      0 192.168.23.42:53        0.0.0.0:*                           0          6181        2052/unbound  
    
Ersichtlich sind 
* 2x recursor: dnscache (127.0.0.1 & 192.168.23.23)
* 1x recursor: unbound (192.168.23.42)
* 2x resolver: tinydns (127.0.0.53 & 192.168.23.42)

dnscache & tinydns sind via daemontools gesteuert. unbound klassischerweise mit sysV.

    ls /etc/service/
    lrwxrwxrwx 1 root root 23 Jun 25 19:23 dnscache-127.0.0.1 -> /var/dnscache-127.0.0.1
    lrwxrwxrwx 1 root root 27 Jun 25 19:24 dnscache-192.168.23.23 -> /var/dnscache-192.168.23.23
    lrwxrwxrwx 1 root root 12 Jun 25 18:47 tinydns -> /var/tinydns
    lrwxrwxrwx 1 root root 22 Jun 25 18:41 tinydns-adblaster -> /var/tinydns-adblaster
    
Kontrolliertes starten und stoppen einzelner oder aller dieser Dienste:

    man svc

Beispiele

    # HUP alle
    svc -h /etc/service/*
    
    # terminate
    svc -t /etc/service/tinydns
    
    # stop / down
    svc -d /etc/service/dnscache-127.0.0.1
    
    # start / up
    svc -u /etc/service/tinydns-adblaster

Weitere Ad-Domain auf den Squid umbiegen
========================================

Ad-Domain abfassen und nach lokal umbiegen:

    cd /etc/service/dnscache-192.168.23.23/root/servers
    cp t21 ${domainname}
    svc -h /etc/service/tinydns-adblaster
    # mit netstat schauen, ob auf 192.168.23.23:53 auch was lauscht ;)

DNS Logs
========

unbound
------- 
    tailf /var/log/syslog | grep unbound

tinydns
-------
    tailf /etc/service/tinydns/log/main/current

bzw.

    tailf /etc/service/tinydns-adblaster/log/main/current

dnscache
--------

    tailf /etc/service/dnscache-192.168.23.23/log/main/current

Squid Caching Proxy
===================

[/etc/squid/squid.conf](squid.conf)

Pic replacement
===============

Alle Bildanfragen auf o.g. adbuster Domains werden von 192.168.23.42 serviert, via nginx. Upload via FTP, per cron generiert ein shellscript je einen html wrapper fuer jedes Bild.

nginx
-----

    nano /etc/nginx/sites-enabled/adblaster

[/etc/nginx/sites-enabled/adblaster](adblaster)

=> 404 => 300 mit eig. document_root (ftp folder)

ftp
---

    # put your pics!
    ftp://anonymous@192.168.23.23/up

imagewrapper
------------

    crontab -u phaidros -e 
    * * * * * /usr/local/bin/htmlimagewiretap

htmlimagewiretap see here: [htmlimagewiretap](htmlimagewiretap)

.. => generates for every image img.jpg in /home/ftp/up/ a file img.jpg.html, which is served by nginx, when ad-images are requested from the users' browser. 