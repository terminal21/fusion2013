```bash
#!/bin/bash

fail() {
                echo "$0: oops"
                exit
}

cd /home/ftp/up || fail
/usr/local/bin/jamendo-rename

shopt -s extglob
for i in $(ls -d !(*.html)); do
        echo $i
        if [ ! -f $i.html ] ; then
                        sed -e "s/XXX.XXX/$i/" ../template.html > $i.html
        fi
done
```

see /usr/local/bin/jamendo-rename

```bash
#!/bin/sh

rename  's/\ /_/g' *
rename  's/-+/-/g' *
rename  's/_-_/-/g' *
rename  "s/\'//g" *
rename  's/[\[\]\(\)\&]//g' *
rename  's/_+/_/g' * 
```