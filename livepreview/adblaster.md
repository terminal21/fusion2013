```bash
server {
        listen   80 default_server; ## listen for ipv4; this line is default and implied
        index index.html index.htm;
        server_name localhost *.com *.net *.org *.de _;
        root   /home/www/adblaster.terminal.21;
        error_log /var/log/nginx/error.log;
        location / {
                root    /home/ftp/up;
                random_index on;
        }
        error_page  404              =302 /img/;
        error_page   500 502 503 504  /errors/50x.html;
        location /doc/ {
                alias /usr/share/doc/;
                autoindex on;
                allow 127.0.0.1;
                allow ::1;
                deny all;
        }
}
```